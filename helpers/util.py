import configparser
import os
import time


def read_config_file(ini_path):
    """
    Reads the ini config file.
    :ini_path: is the path of the file.
    :param ini_path: .ini file path
    :return: Dictionary with section:option keys/values.
    """
    config_file = configparser.RawConfigParser()
    config_file.optionxform = str
    config_file.read(ini_path)

    # Create dict for config file information.
    config_info = {}

    # Iterate config file sections.
    for section in config_file.sections():
        config_info[section] = {}

        # Iterate options for each section and save into the dict.
        for option in config_file.options(section):
            config_info[section][option] = config_file.get(section, option)

    return config_info


def get_browser_options_and_profiles(rows: object):
    browser_options_list = {}
    firefox_option_list = {}
    mobile_emulation = {}
    result = {}
    is_browser_option = False
    is_firefox_profile_options = False
    is_mobile_emulation = False

    for r in rows:
        if "@CHROME_OPTIONS" in r[0] or "@FIREFOX_OPTIONS" in r[0]:
            is_browser_option = True
            is_firefox_profile_options = False
            continue
        elif "@PROFILE_OPTIONS" in r[0]:
            is_firefox_profile_options = True
            is_browser_option = False
            continue
        elif "@MOBILE_EMULATION" in r[0]:
            is_mobile_emulation = True
            is_firefox_profile_options = False
            is_browser_option = False
            continue

        if is_browser_option:
            browser_options_list[r[0]] = r[1]
        elif is_firefox_profile_options:
            firefox_option_list[r[0]] = r[1]
        elif is_mobile_emulation:
            mobile_emulation[r[0]] = r[1]

    browser_options_list["mobile_emulation_options"] = mobile_emulation
    result["browser_options"] = browser_options_list
    result["firefox_profile"] = firefox_option_list if firefox_option_list != {} else None

    return result


def process_browser_options_step_table(context: object):
    for i, r in enumerate(context.table.rows):
        if r.cells[0].lower() == "browser_brand":
            context.execution_data["browser"]["browser_brand"] = r.cells[1]
        elif "@" in r.cells[0]:
            options_and_profiles = get_browser_options_and_profiles(context.table.rows[i::])
            context.execution_data["browser"]["options"] = options_and_profiles.get("browser_options")
            context.execution_data["browser"]["firefox_profile"] = options_and_profiles.get("firefox_profile")
            break


def take_screenshot(context: object):
    """
    Takes screenshot from the UI.
    :param context: Behave context variable
    :param junit_xml_name: Screenshot folder name
    :return: None
    """
    time_stamp = get_time_stamp()

    screenshot_directory = context.cfg.get("behave").get("junit_directory") + "screenshots/" \
                           + "passed/" if context.scenario.status.name == "passed" else "failed/"

    # Screenshot folder destiny
    screenshot_folder = convert_slash_path(context.execution_data["project_root"] + "\\"
                                           + screenshot_directory) + context.feature_name.replace(" ", "_") + "/"

    create_folder(screenshot_folder)
    driver = context.execution_data.get("browser").get("driver")

    # Create screenshot filename
    screenshot_file_name = context.scenario_name.replace(" ", "_") + "-" + time_stamp + '.png'

    # Save screenshot on specified folder
    try:
        driver.save_screenshot(screenshot_folder + screenshot_file_name)
    except Exception as e:
        raise Exception(4 * "<" + "[ERROR]" + 4 * ">" + "An error occurred when trying to take a screenshot: "
                        + "\nERROR: "
                        + format(e)
                        + "\n")


def create_folder(file_path):
    import os
    import errno

    # Check platform and convert \ to / in file path
    file_path = convert_slash_path(file_path)

    try:
        os.mkdir(file_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise Exception("Error when creating a folder in " + file_path + "\n")


def convert_slash_path(f_path):
    """
        Checks platform and performs conversion from windows path to mac & linux ones if needed.
        :param f_path:
        :return:
    """
    import platform

    if platform.system().lower() != "windows":
        f_path = f_path.replace("\\", "/")

    return f_path


def get_time_stamp():
    """
    Returns time stamp
    :return: Time stamp format mdY-HMS
    """
    timestr = time.strftime("%m%d%Y-%H%M%S")
    time_stamp = ""
    for i, t in enumerate(timestr):
        if i == 2 or i == 4:
            time_stamp = time_stamp + "-"
        time_stamp = time_stamp + t
    return time_stamp


def close_and_quit_driver(driver):
    try:
        driver.quit()
    except Exception as e:
        print(
            4 * "<" + "[ERROR]" + 4 * ">" + "An error occurred in after_scenario() method: "
            + "\nERROR: "
            + format(e)
            + "\n")


def read_json_file(file_path):
    import json
    try:
        with open(file_path, "r") as file:
            file = json.load(file)
    except FileNotFoundError as e:
        raise Exception(
            4 * "<" + "[ERROR]" + 4 * ">" + "An error occurred when trying to read  "
            + file_path
            + " file"
            + "\nERROR: "
            + format(e)
            + "\n")

    return file


def table_to_dict(table, transposed=False):
    """
    Convert Step Table to Dictionary
    :param table: Behave feature table data format
    :param transposed: If step data table is transposed.
    :return: dictionary when the table is not transposed and tuple when it is transposed
    """
    test_data = []
    if not transposed:
        for row in table:
            test_data.append([str(row[0]), str(row[1])])
        return dict(test_data)
    else:
        heading = table.headings
        for r in table.rows:
            for i, v in enumerate(r):
                test_data.append((heading[i], v))
        return tuple(test_data)