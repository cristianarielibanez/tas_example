import os
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium import webdriver
from selenium.common.exceptions import WebDriverException


class BrowserDriver:

    __instance = None
    # Browsers path
    head, tail = os.path.split(os.path.dirname(os.path.abspath(__file__)))
    __chrome_driver_windows_path = head + "\lfs\webdriver\chromedriver.exe"
    __firefox_driver_windows_path = head + "\lfs\webdriver\geckodriver.exe"
    __phantomjs_driver_windows_path = head + "\lfs\webdriver\phantomjs.exe"

    # Chrome Options
    # URL: https://chromium.googlesource.com/chromium/src/+/master/chrome/common/chrome_switches.cc
    # URL: https://chromium.googlesource.com/chromium/src/+/master/chrome/common/pref_names.cc
    CHROME_HEADLESS = "--headless"
    CHROME_WINDOWS_SIZE = "----window-size="
    CHROME_DISABLE_EXTENSIONS = "disable-extensions"
    CHROME_START_MAXIMIZED = "--start-maximized"
    CHROME_DISABLE_COMPONENT_EXTENSIONS_WITH_BACKGROUND_PAGES= "disable-component-extensions-with-background-pages"
    CHROME_DISABLE_PROMPT_ON_REPOST = "disable-prompt-on-repost"
    CHROME_KEEP_ALIVE_FOR_TEST = "keep-alive-for-test"
    CHROME_MOBILE_EMULATION = "mobileEmulation"

    # FIREFOX OPTIONS
    FF_HEADLESS = "-headless"

    def __new__(cls):
        if BrowserDriver.__instance is None:
            BrowserDriver.__instance = object.__new__(cls)

        return BrowserDriver.__instance

    @classmethod
    def get_browser_driver(cls, browser_name, options=None, firefox_profile=None, platform="WINDOWS"):
        driver_path = cls.__get_driver_path(browser_name, platform=platform)

        if browser_name.upper() == "CHROME":
            # if options and str(type(options)).endswith("chrome.options.Options", 8, 49):
            if options and str(type(options)).endswith('dict', 8, 12):
                options = cls.__get_chrome_options(options_list=options)

            return cls.get_chrome_driver(driver_path=driver_path, chrome_options=options)
        elif browser_name.upper() in ["FF", "FIREFOX"]:
            if options:
                options = cls.__get_firefox_options(options_list=options)
            if firefox_profile:
                firefox_profile = cls.__get_firefox_profile(firefox_profile)
            return cls.get_firefox_driver(driver_path=driver_path, firefox_options=options, firefox_profile=firefox_profile)
        elif browser_name.upper() == "PHANTOMJS":
            return cls.get_phantomjs_driver(driver_path=driver_path)
        elif browser_name.upper() == "SAFARI":
            return cls.get_safari_driver()
        else:
            raise Exception("Not Supported Browser: " + browser_name)

    @classmethod
    def get_chrome_driver(cls, driver_path, chrome_options=None):
        try:
            driver = webdriver.Chrome(driver_path, chrome_options=chrome_options)
        except WebDriverException as e:
            raise Exception(4 * "<" + "[ERROR]" + 4 * ">"
                            + "An error occurred when trying to create an Chrome Webdriver instance."
                            + "\nERROR: " + format(e)
                            + "\n")

        return driver

    @classmethod
    def __get_driver_path(cls, browser_name: str, platform="WINDOWS"):
        if browser_name.upper() == "CHROME":
            if platform.upper() == "WINDOWS":
                return cls.__chrome_driver_windows_path
        elif browser_name.upper() == "FIREFOX":
            if platform.upper() == "WINDOWS":
                return cls.__firefox_driver_windows_path
        elif browser_name.upper() == "PHANTOMJS":
            if platform.upper() == "WINDOWS":
                return cls.__phantomjs_driver_windows_path
        elif browser_name.upper() == "SAFARI":
            if platform.upper() == "WINDOWS":
                return None
        else:
            raise Exception("Not Supported Browser: " + platform)

    @classmethod
    def __get_chrome_options(cls, options_list: dict):
        c_options = []
        mobile_emulation = {}

        for o in options_list:
            if o == "headless":
                if options_list.get(o).upper() in ["TRUE", "YES"]:
                    c_options.append(cls.CHROME_HEADLESS)
            elif o == "windows_size":
                c_options.append(cls.CHROME_WINDOWS_SIZE + options_list[o])
            elif o == "disable_extensions":
                c_options.append(cls.CHROME_DISABLE_EXTENSIONS)
            elif o == "start_maximized":
                c_options.append(cls.CHROME_START_MAXIMIZED)
            elif o == "disable_component_extensions_with_background_pages":
                c_options.append(cls.CHROME_DISABLE_COMPONENT_EXTENSIONS_WITH_BACKGROUND_PAGES)
            elif o == "disable_prompt_on_repost":
                c_options.append(cls.CHROME_DISABLE_PROMPT_ON_REPOST)
            elif o == "keep_alive_for_test":
                c_options.append(cls.CHROME_KEEP_ALIVE_FOR_TEST)
            elif o == "mobile_emulation":
                if options_list.get(o).upper() in ["TRUE", "YES"]:
                    for m in options_list['mobile_emulation_options']:
                        mobile_emulation[m] = options_list['mobile_emulation_options'][m]
            elif o == "mobile_emulation_options":
                continue
            else:
                raise Exception("Not Supported Chrome Option: ", o)

        return cls.create_chrome_options(c_options, mobile_emulation)

    @classmethod
    def create_chrome_options(cls, c_options, mobile_emulation=None):
        chrome_options = ChromeOptions()
        for o in c_options:
            chrome_options.add_argument(o)

        if mobile_emulation:
            chrome_options.add_experimental_option(cls.CHROME_MOBILE_EMULATION, mobile_emulation)

        return chrome_options

    @classmethod
    def __get_firefox_options(cls, options_list: dict):
        ff_options_list = []

        for o in options_list:
            if o == "headless":
                if options_list.get(o).upper() in ["TRUE", "YES"]:
                    ff_options_list.append(cls.FF_HEADLESS)
            elif o == "mobile_emulation_options":
                continue
            else:
                raise Exception("Not Supported Chrome Option: ", o)

        firefox_options = FirefoxOptions()
        for o in ff_options_list:
            firefox_options.add_argument(o)

        return firefox_options

    @classmethod
    def __get_firefox_profile(cls, f_profile_options: dict):
        firefox_profile = webdriver.FirefoxProfile()
        for o in f_profile_options:
            firefox_profile.set_preference(o, f_profile_options[o])

        return firefox_profile

    @classmethod
    def get_firefox_driver(cls, driver_path, firefox_options=None, firefox_profile=None):
        try:
            driver = webdriver.Firefox(firefox_profile=firefox_profile, executable_path=driver_path,
                                       firefox_options=firefox_options)

        except WebDriverException as e:
            raise Exception(4 * "<" + "[ERROR]" + 4 * ">"
                            + "An error occurred when trying to create an FF Webdriver instance."
                            + "\nERROR: "
                            + format(e)
                            + "\n")

        return driver

    @classmethod
    def get_safari_driver(cls):
        try:
            driver = webdriver.Safari()

        except WebDriverException as e:
            raise Exception(4 * "<" + "[ERROR]" + 4 * ">"
                            + "An error occurred when trying to create an FF Webdriver instance."
                            + "\nERROR: "
                            + format(e)
                            + "\n")
        return driver

    @classmethod
    def get_phantomjs_driver(cls, driver_path):
        try:
            driver = webdriver.PhantomJS(executable_path=driver_path)

        except WebDriverException as e:
            raise Exception(4 * "<" + "[ERROR]" + 4 * ">"
                            + "An error occurred when trying to create an FF Webdriver instance."
                            + "\nERROR: "
                            + format(e)
                            + "\n")
        return driver
