from helpers.browser_driver import BrowserDriver
from retry.api import retry


class BrowserFactory:
    __instance = None

    #
    IMPLICIT_WAIT = 0
    PAGE_LOAD_TIMEOUT = 60

    def __init__(self):
        pass

    def __new__(cls):
        if BrowserFactory.__instance is None:
            BrowserFactory.__instance = object.__new__(cls)

        return BrowserFactory.__instance

    @classmethod
    @retry(exceptions=Exception, tries=5, delay=1)
    def get_driver(cls, browser_name, options=None, firefox_profile=None, platform="WINDOWS"):
        driver = BrowserDriver.get_browser_driver(browser_name=browser_name, options=options,
                                                  firefox_profile=firefox_profile, platform=platform)

        driver.delete_all_cookies()
        driver.implicitly_wait(cls.IMPLICIT_WAIT)
        driver.set_page_load_timeout(cls.PAGE_LOAD_TIMEOUT)
        return driver
