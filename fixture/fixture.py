from behave import fixture
from helpers.browser_factory import BrowserFactory as bf
from helpers import util


@fixture
def instantiate_browser(context):
    # -- SETUP-FIXTURE PART:
    context.execution_data["browser"]["driver"] = bf.get_driver(
        browser_name=context.execution_data["browser"]["browser_brand"],
        options=context.execution_data["browser"]["options"],
        platform=context.execution_data["platform"])

    # Register as context-cleanup task.
    context.add_cleanup(context.execution_data["browser"]["driver"].quit)
    return context.execution_data["browser"]["driver"]
