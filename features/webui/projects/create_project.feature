Feature: Projects
  As a user
  I want to create a Project and check that it was successfully created.

  @Working
  Scenario Outline: <browser_brand> - Create a Project with Mock Data
    Given Set up browser
      | field           | data             |
      | browser_brand   | <browser_brand>  |
      | @CHROME_OPTIONS | ################ |
      | headless        | False            |
    Given I connect to redmine
    When Login into redmine
      | field    | data     |
      | user     | user     |
      | password | 12345678 |
    When I create a project
      | field                 | data      |
      | project_name          | mock_data |
      | description           | mock_data |
      | identifier            | mock_data |
      | homepage              | mock_data |
      | public                | True      |
      | sub_project_of        | QAA       |
      | inherit_members       | False     |
      | module_issue_tracking | False     |
      | module_time_tracking  | False     |
      | module_news           | False     |
      | module_documents      | False     |
      | module_files          | False     |
      | module_wiki           | False     |
      | module_repository     | False     |
      | module_forums         | False     |
      | module_calendar       | False     |
      | module_gantt          | False     |

    Then The Project was successfully created
    Then The Project should be available in Projects list

    Examples: Crossbrowser testing
      | browser_brand |
      | Chrome        |

  @Working
  Scenario Outline: <browser_brand> - Create a Project
    Given Set up browser
      | field           | data             |
      | browser_brand   | <browser_brand>  |
      | @CHROME_OPTIONS | ################ |
      | headless        | False            |
    Given I connect to redmine
    When Login into redmine
      | field    | data     |
      | user     | user     |
      | password | 12345678 |
    When I create a project
      | field                 | data           |
      | project_name          | <project_name> |
      | description           | <description>  |
      | identifier            | <identifier>   |
      | homepage              | <homepage>     |
      | public                | True           |
      | sub_project_of        | QAA            |
      | inherit_members       | False          |
      | module_issue_tracking | False          |
      | module_time_tracking  | False          |
      | module_news           | False          |
      | module_documents      | False          |
      | module_files          | False          |
      | module_wiki           | False          |
      | module_repository     | False          |
      | module_forums         | False          |
      | module_calendar       | False          |
      | module_gantt          | False          |

    Then The Project was successfully created
    Then The Project should be available in Projects list

    Examples: Crossbrowser testing
      | browser_brand | project_name | description              | identifier | homepage                |
      | Chrome        | mock_data    | mock_data                | mock_data  | mock_data               |
      | Chrome        | Quantum      | This project is about... | Quantum    | https://www.quantum.com |
