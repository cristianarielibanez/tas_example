Feature: Login - Simple login test
  As a user
  I want to login to the application.

  @Working
  Scenario Outline: <browser_brand> - User inputs correct username and password
    Given Set up browser
      | field           | data             |
      | browser_brand   | <browser_brand>  |
      | @CHROME_OPTIONS | ################ |
      | headless        | False            |
    Given I connect to redmine
    When Login into redmine
      | field    | data     |
      | user     | user     |
      | password | 12345678 |
    Then "user" user should be logged in

    Examples: Crossbrowser testing
      | browser_brand |
      | Chrome        |
      | Firefox       |
