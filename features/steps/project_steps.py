from behave import *
from page_objects.projects_po import ProjectsPage
from helpers.mock_data import Generator
from assertpy import assert_that, soft_assertions
import copy
from helpers import util


@when("I create a project")
def step_impl(context):
    driver = context.execution_data["browser"]["driver"]
    context.homepage_po.homepage_breadcrumb_po.click_on_option("Projects")
    context.model = copy.deepcopy(context.table)
    context.td = util.table_to_dict(context.model, transposed=False)

    # Getting test data
    if context.td.get("project_name", "").lower() == "mock_data":
        project_name = Generator.generate_sentence(nb_words=6)
        context.td["project_name"] = project_name
    else:
        from random import randint
        context.td["project_name"] = context.td["project_name"] + "-" + str(randint(0, 9999999))

    if context.td.get("description", "").lower() == "mock_data":
        description = Generator.generate_text(max_nb_chars=100)
        context.td["description"] = description

    if context.td.get("identifier", "").lower() == "mock_data":
        identifier = context.td["identifier"] + Generator.generate_license_plate_number()
        context.td["identifier"] = identifier

    if context.td.get("homepage", "").lower() == "mock_data":
        homepage = Generator.generate_profile().get("website")[0]
        context.td["homepage"] = homepage

    context.project_po = ProjectsPage(driver)
    context.project_po.create_project(context.td)


@then("The Project was successfully created")
def step_impl(context):
    ui_message = context.project_po.get_ui_message()

    with soft_assertions():
        assert_that(ui_message).is_equal_to("Successful creation.").described_as("The message was not found or was incorrect.")


@then("The Project should be available in Projects list")
def step_impl(context):
    context.homepage_po.homepage_breadcrumb_po.click_on_option('Projects')

    projects_list = context.project_po.get_project_list()

    with soft_assertions():
        assert_that(context.td["project_name"] in projects_list).is_true().described_as("The Project was not found.")
