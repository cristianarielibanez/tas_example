from behave import *
from page_objects.login_po import LoginPO
from page_objects.homepage_po import HomepagePO
from helpers import util


@given("Set up browser")
def step_impl(context):
    util.process_browser_options_step_table(context)


@given("I connect to redmine")
def step_impl(context):
    from behave import use_fixture
    from fixture import fixture

    if 'fixture.browser.options.json' not in context.scenario.tags:
        use_fixture(fixture.instantiate_browser, context)

    context.execution_data["browser"]["driver"].get(getattr(context, "execution_data").get("sut_url"))


@when("Login into redmine")
def step_impl(context):
    user = context.table.rows[0].cells[1]
    password = context.table.rows[1].cells[1]
    driver = context.execution_data["browser"]["driver"]

    context.homepage_po = HomepagePO(driver)
    context.homepage_po.click_breadcrumb_option("sign in")

    login_po = LoginPO(driver=driver)
    login_po.login(user=user, password=password)


@then('"{user_name}" user should be logged in')
def step_impl(context, user_name):
    logged_as = context.homepage_po.get_logged_as_text()
    driver = context.execution_data["browser"]["driver"]
    assert logged_as == "Logged in as " + user_name
    assert driver.title == "Redmine"
