from page_objects.base_po import BasePO
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


class ProjectsPage(BasePO):
    # Defining locators and webelements.
    __btn_new_project_loc = (By.XPATH, "//a[text()='New project']")

    __txt_project_name_loc = (By.ID, "project_name")

    __txt_project_description_loc = (By.ID, "project_description")

    __txt_project_identifier_loc = (By.ID, "project_identifier")

    __txt_homepage_loc = (By.ID, "project_homepage")

    __chk_public_loc = (By.ID, "project_is_public")

    __sel_subproject_of_loc = (By.ID, "project_parent_id")

    __chk_inherit_members_loc = (By.ID, "project_inherit_members")

    __chk_modules_list_loc = (By.XPATH, "//legend[text()='Modules']/parent::fieldset//input[@type='checkbox']")

    __chk_trackers_list_loc = (By.XPATH, "//legend[text()='Trackers']/parent::fieldset//input[@type='checkbox']")

    __btn_create_loc = (By.NAME, "commit")

    __btn_create_and_continue_loc = (By.NAME, "continue")

    __btn_save_loc = (By.XPATH, "//form[@id='edit_project_5']/input[@value='Save']")

    __lbl_message_loc = (By.ID, "flash_notice")

    __tbl_projects_list_loc = (By.XPATH, "//*[@id='projects-index']//a")

    def __init__(self, driver):
        super(ProjectsPage, self).__init__(driver=driver)

    def create_project(self, td):
        create_project_we = self.wait.until(EC.presence_of_element_located((self.__btn_new_project_loc[0],
                                                                            self.__btn_new_project_loc[1])))
        create_project_we.click()

        self.driver.find_element(self.__txt_project_name_loc[0], self.__txt_project_name_loc[1]).send_keys(td.get("project_name"))

        if td.get("description", None):
            self.driver.find_element(self.__txt_project_description_loc[0], self.__txt_project_description_loc[1]) \
                .send_keys(td["description"])

        # MODULES
        check_boxes_list = self.driver.find_elements(self.__chk_modules_list_loc[0], self.__chk_modules_list_loc[1])
        for k, v in td.items():
            if k.startswith("module_"):
                self.click_check_box_by_name(elements=check_boxes_list,
                                             checkbox_name=k.replace("module_", ""))

        if td.get("homepage", None):
            self.driver.find_element(self.__txt_homepage_loc[0], self.__txt_homepage_loc[1]).send_keys(td.get("homepage"))

        if td.get("public", None):
            self.click_check_box(self.driver.find_element(self.__chk_public_loc[0], self.__chk_public_loc[1]))

        self.driver.find_element(self.__btn_create_loc[0], self.__btn_create_loc[1]).click()

    def get_ui_message(self):
        element = self.wait.until(EC.presence_of_element_located((self.__lbl_message_loc[0], self.__lbl_message_loc[1])))
        return element.text

    def save_project(self):
        self.driver.find_element(self.__btn_save_loc[0], self.__btn_save_loc[1]).click()

    def get_project_list(self):
        p_list = []
        table_projects_we = self.driver.find_elements(self.__tbl_projects_list_loc[0], self.__tbl_projects_list_loc[1])

        for p in table_projects_we:
            p_list.append(p.text)

        return p_list
