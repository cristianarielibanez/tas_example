from selenium.webdriver.support.ui import WebDriverWait


class BasePO:
    driver: object

    def __init__(self, driver: object):
        self.driver = driver
        self.wait = WebDriverWait(driver=driver, timeout=10, poll_frequency=0.5)

    def click_check_box(self, element: object, uncheck=False):
        if (not element.is_selected() and not uncheck) or (element.is_selected() and uncheck):
            element.click()

    def click_check_box_by_name(self, elements: object, checkbox_name: str):
        for chk in elements:
            if chk.get_attribute('value') == checkbox_name:
                chk.click()
                break


