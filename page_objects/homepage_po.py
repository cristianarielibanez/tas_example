from page_objects.base_po import BasePO
from page_objects.homepage_breadcrumb_po import HomepageBreadcrumbPO


class HomepagePO (BasePO):
    def __init__(self, driver):
        super(HomepagePO, self).__init__(driver)
        self.homepage_breadcrumb_po = HomepageBreadcrumbPO(driver)

    def click_breadcrumb_option(self, option: str):
        self.homepage_breadcrumb_po.click_on_option(option)

    def get_logged_as_text(self):
        return self.homepage_breadcrumb_po.get_logged_user_label()
