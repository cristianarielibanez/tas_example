from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import ElementNotVisibleException
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_po import BasePO
from selenium.webdriver.common.by import By
from page_objects.homepage_po import HomepagePO


class LoginPO(BasePO):

    # Locators section
    btn_sign_in_loc = (By.XPATH, "//a[@class='login']")

    txt_user_name_loc = (By.ID, "username")

    txt_password_loc = (By.ID, "password")

    btn_login_loc = (By.ID, "login-submit")

    login_panel_loc = (By.ID, "login-form")

    def __init__(self, driver):
        super(LoginPO, self).__init__(driver)

    def login(self, user, password):
        wait = WebDriverWait(driver=self.driver, timeout=10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException])
        user_name_we = wait.until(EC.presence_of_element_located((self.txt_user_name_loc[0], self.txt_user_name_loc[1])))
        user_name_we.send_keys(user)

        # self.driver.find_element(self.txt_user_name_loc[0], self.txt_user_name_loc[1]).send_keys(user)
        self.driver.find_element(self.txt_password_loc[0], self.txt_password_loc[1]).send_keys(password)
        self.driver.find_element(self.btn_login_loc[0], self.btn_login_loc[1]).click()

