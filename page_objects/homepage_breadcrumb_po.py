from page_objects.base_po import BasePO
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class HomepageBreadcrumbPO(BasePO):
    link_loc = "//a[@href='{0}']"
    link_url = {
        "HOME": "/",
        "MY PAGE": "/my/page",
        "PROJECTS": "/projects",
        "ADMINISTRATION": "/admin",
        "HELP": "https://www.redmine.org/guide",
        "MY ACCOUNT": "/my/account",
        "SIGN IN": "/login",
        "REGISTER": "/account/register",
        "SIGN OUT": "/logout"
    }

    logged_as_loc = (By.ID, "loggedas")

    def __init__(self, driver):
        super(HomepageBreadcrumbPO, self).__init__(driver)

    def click_on_option(self, option: str):
        link_we = self.driver.find_element(By.XPATH, self.link_loc.format(self.link_url.get(option.upper())))
        link_we.click()

    def get_logged_user_label(self):
        # wait = WebDriverWait(driver=self.driver, timeout=10, poll_frequency=0.5)

        we = self.wait.until(EC.presence_of_element_located((self.logged_as_loc[0], self.logged_as_loc[1])))

        return we.text


